import Login from '@/views/Login.vue';
import SignUp from '@/views/SignUp.vue';

export const routes = [
    { path: '/home', component: Login },
    { path: '/login', component: Login },
    { path: '/signup', component: SignUp }
];