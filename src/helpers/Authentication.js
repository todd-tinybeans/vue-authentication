import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import ENV from '@/config/environment';

Vue.use(VueAxios, axios);

const authenticateWithTinybeans = (credentials) => {
  return new Promise((resolve, reject) => {
    const credentialsObj = { ...credentials };
  
    credentialsObj.clientId = ENV.clientId;
    credentialsObj.deviceId = localStorage.getItem('__leanplum_device_id');

    Vue.axios.post(ENV.apis.authentication, credentialsObj)
    .then((res) => {
      res.rememberMe = credentialsObj.rememberMe;
      res.access_token = res.accessToken;
      resolve(res);
    })
    .catch((error) => {
      reject(error);
    });
  });
};

const authenticateWithFacebook = () => {

};

export {
  authenticateWithTinybeans,
  authenticateWithFacebook
};

