const emailValidation = (email) => {
  const emailFormatReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/;
  return emailFormatReg.test(email) ? true : false;
};

const readCookie = (key) => {
  const value = document.cookie.match(new RegExp(key + '=([^;]+)')) || [];
  return decodeURIComponent(value[1] || '');
};
const writeCookie = (key, value, expiration) => {
  //to-do
  //Need to re-write this function
  // const expires = expiration ? '' : '; expires=' + new Date(expiration).toUTCString();
  // const secure = !!this._secureCookies ? ';secure' : '';
  // const path = ";path=/";
  // document.cookie = key + '=' + encodeURIComponent(value) + expires + secure + path;
};

export {
  emailValidation,
  readCookie,
  writeCookie
};