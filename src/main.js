import Vue from 'vue';
import VueRouter from 'vue-router';

import App from '@/App.vue';
import Init from '@/helpers/Init';
import { routes } from '@/routes/index';

Vue.config.productionTip = false;
Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history' //to remove # in url
});

Init();

new Vue({
  render: h => h(App),
  router
}).$mount('#app');
